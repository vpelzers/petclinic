import fileinput
import os

ip_address = os.environ.get('ip_address')

# Replace the IP address in the JMeter test file with the new value
for line in fileinput.input('loadtesting.jmx', inplace=True):
    print(line.replace('$(ip_address)', ip_address), end='')
