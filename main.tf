
data "azurerm_resource_group" "RG" {
  name = "valeria_pelzers-rg"
}

#data "azurerm_virtual_network" "VNET" {
# name                = "vp-vnet"
# resource_group_name = data.azurerm_resource_group.RG.name 
#}
resource "azurerm_virtual_network" "VNET" {
  name                = "VNET-test"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.RG.location
  resource_group_name = data.azurerm_resource_group.RG.name 

  tags = {
    Pillar = "M Cloud"
    Role   = "Futures"
    Usage  = "Training / Certification related activities"
  }
}
#data "azurerm_subnet" "subnet" {
#  name = "vp-subnet"
#  virtual_network_name = data.azurerm_virtual_network.VNET.name
#  resource_group_name  = data.azurerm_resource_group.RG.name 
#}
resource "azurerm_subnet" "subnet" {
  name                 = "SUBNET-test"
  resource_group_name  = data.azurerm_resource_group.RG.name 
  virtual_network_name = azurerm_virtual_network.VNET.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_security_group" "SG" {
  name                = "SG-test"
  location            = data.azurerm_resource_group.RG.location
  resource_group_name = data.azurerm_resource_group.RG.name 

  tags = {
    Pillar = "M Cloud"
    Role   = "Futures"
    Usage  = "Training / Certification related activities"
  }
}

resource "azurerm_network_security_rule" "Inbound22" {
  name                        = "Allow22Inbound"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = data.azurerm_resource_group.RG.name 
  network_security_group_name = azurerm_network_security_group.SG.name
}
  resource "azurerm_network_security_rule" "Inbound4141" {
  name                        = "Allow4141Inbound"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "4141"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = data.azurerm_resource_group.RG.name 
  network_security_group_name = azurerm_network_security_group.SG.name
}

resource "azurerm_subnet_network_security_group_association" "NSGA" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.SG.id
}

resource "azurerm_public_ip" "IP" {
  name                = "IP-test"
  resource_group_name = data.azurerm_resource_group.RG.name 
  location            = data.azurerm_resource_group.RG.location
  allocation_method   = "Static"

  tags = {
    Pillar = "M Cloud"
    Role   = "Futures"
    Usage  = "Training / Certification related activities"
  }
}
resource "azurerm_network_interface" "nic" {
  name                = "NIC-test"
  location            = data.azurerm_resource_group.RG.location
  resource_group_name = data.azurerm_resource_group.RG.name 


  tags = {
    Pillar = "M Cloud"
    Role   = "Futures"
    Usage  = "Training / Certification related activities"
  }

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.IP.id
  }
}

variable "public_key"{
    type = string
  }
  
resource "azurerm_linux_virtual_machine" "vm" {
  name                            = "testvm"
  resource_group_name             = data.azurerm_resource_group.RG.name 
  location                        = data.azurerm_resource_group.RG.location
  size                            = "Standard_B2ms"
  admin_username                  = "testuser"
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]
  tags = {
    Pillar = "M Cloud"
    Role   = "Futures"
    Usage  = "Training / Certification related activities"
  }
  
  admin_ssh_key {
    username = "testuser"
    public_key = var.public_key
}

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202209200"
  }
}

