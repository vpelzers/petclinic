
FROM tomcat:9-jdk11-openjdk-slim
COPY /target/petclinic.war /usr/local/tomcat/webapps
EXPOSE 4141
#CMD ["catalina.sh", "run", "-Dcatalina.http.port=4040"]


