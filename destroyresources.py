from azure.identity import DefaultAzureCredential
from azure.mgmt.resource import ResourceManagementClient

# Replace <subscription-id>, <resource-group-name>, and <region> with your values
subscription_id = '41e50375-b926-4bc4-9045-348f359cf721'
resource_group_name = 'valeria_pelzers-rg'
region = 'West Europe'

# Create a ResourceManagementClient using the default credentials
credential = DefaultAzureCredential()
resource_client = ResourceManagementClient(credential, subscription_id)

# Delete all resources within the resource group
resources = resource_client.resources.list_by_resource_group(resource_group_name)
for resource in resources:
    resource_client.resources.delete_by_id(resource.id, region)
