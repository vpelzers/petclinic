
terraform {
    backend "azurerm" {
        resource_group_name = "valeria_pelzers-rg"
        storage_account_name = "vpelzersstorage"
        container_name = "tfstate"
        key = "terraform.statefile"
    }
}